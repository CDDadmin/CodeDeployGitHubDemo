#!/bin/bash

set -eu
echo "Home=$HOME"

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"

codedeploy_application="S3TestingApp"
codedeploy_deployment_group="S3Testing_DG"
revision_bundle_type="zip"
revision_s3_bucket_name="codedeploybucketcdd"
revision_s3_key="SampleAppGitlab.${revision_bundle_type}"
revision_s3_location="s3://${revision_s3_bucket_name}/${revision_s3_key}"
project_dir="$(readlink -f ${__dir}/../../)"

generate_revision_description() {
  echo "This is a revision for the python-django application"
}

generate_deployment_description() {
  echo "This is a revision for the python-django application"
}

create_codedeploy_revision() {
  echo "Uploading revision to S3 ..."
  deployment_s3_location=$(aws deploy push \
    --application-name "${codedeploy_application}" \
    --description "$(generate_revision_description)" \
    --ignore-hidden-files \
    --s3-location "${revision_s3_location}" \
    --source . | grep -Po '\-\-s3-location \K[^ ]+')
  echo "Codedeploy revision for S3 location ${revision_s3_location} is created"
}

main() {
  echo "
Starting the deployment to codedeploy
-----------------------------------------------------------------
Codedeploy Application: ${codedeploy_application}
Codedeploy Deployment Group: ${codedeploy_deployment_group}
Revision S3 Location: ${revision_s3_location}
Source Location: ${project_dir}
------------------------------------------------------------------
  "
  create_codedeploy_revision
}

main
